function reduce(elements, cb, startingValue) 
{
    let sum=0
    if(startingValue === undefined)
    {
        sum=elements[0]
        for(let i in elements)
        {
            sum=cb(elements[i],sum)
        }
    }
    else
    {
        sum =startingValue
        for(let i in elements)
        {
            sum=cb(elements[i],sum)
        }
    }
    return sum
}
export{
    reduce
};
